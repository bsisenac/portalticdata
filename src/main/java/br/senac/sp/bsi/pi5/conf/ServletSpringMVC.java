package br.senac.sp.bsi.pi5.conf;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Em projetos Web seguindo a especificação de Servlets,
 * é necessário ter no mínim um Filter ou uma Servlet configurada.
 * <P>
 * A Servlet responsável por tratar todas as requisições que chegam
 * para o Spring MVC é a DispatcherServlet.
 * <P>
 * A maneira tradicional de configurar a Servlet é através do
 * arquivo web.xml. Neste caso, a configuração será realizada por
 * esta classe.
 *
 */
public class ServletSpringMVC extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {AppWebConfiguration.class, JPAConfiguration.class};
	}

	/**
	 * Informa ao Spring MVC quais os controllers devem ser mapeados e quais outras
	 * classes devem ser carregadas pelo container.
	 * O método retorna uma ou mmais classes responsáveis por indicar quais outras classes
	 * devem ser lidas durante o carregamento da aplicação web.
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {};
	}

	/**
	 * Neste método você diz qual é o padrão de endereço que vai ser delegado para a 
	 * Servlet do Spring MVC. Se utilizássemos o arquivo web.xml a configuraçã seria realizada
	 * em <url-mapping>.
	 *  
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"} ;
	}

}
