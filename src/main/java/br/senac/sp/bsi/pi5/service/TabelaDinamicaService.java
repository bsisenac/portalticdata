package br.senac.sp.bsi.pi5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senac.sp.bsi.pi5.base.service.BaseServiceImpl;
import br.senac.sp.bsi.pi5.dao.TabelaDAO;
import br.senac.sp.bsi.pi5.dao.TabelaDinamicaDAO;
import br.senac.sp.bsi.pi5.model.TabelaDinamicaEntidade;

@Service
public class TabelaDinamicaService extends BaseServiceImpl<TabelaDinamicaEntidade>{

	@Autowired
	public TabelaDinamicaDAO tabelaDAO;
	
}
