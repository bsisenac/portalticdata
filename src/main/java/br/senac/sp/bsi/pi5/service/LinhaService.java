package br.senac.sp.bsi.pi5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.senac.sp.bsi.pi5.base.dao.BaseDAOImpl;
import br.senac.sp.bsi.pi5.dao.LinhaDAO;
import br.senac.sp.bsi.pi5.model.LinhaEntidade;

@Service
@Transactional
public class LinhaService extends BaseDAOImpl<LinhaEntidade> {
	
	@Autowired
	public LinhaDAO linhaDAO;
	
}
