<div class="col-sm-3">
	<div class="list-group no-print">

		<!-- Menu Level 01 Dashboard -->
		<a href="#submenu-01" class="list-group-item" data-toggle="collapse">
			<span class="glyphicon glyphicon-home pull-right"></span> Dashboard
		</a>
		<ul id="submenu-01" class="sub-nav collapse in">
			<li>
				<a href="/index.jsp" title="Dashboard">
					Home
				</a>
			</li>
		</ul>
		<!-- Menu Level 01 -->

		<!-- Menu Level 02 Pesquisas -->
		<a href="#submenu-02" class="list-group-item" data-toggle="collapse">
			<span class="glyphicon glyphicon-signal pull-right"></span> Pesquisas
		</a>
		<ul id="submenu-02" class="sub-nav collapse in">
			<li>
				<a href="<c:url value='/pesquisas' />" title="Listar pesquisas">
				Lista de pesquisas 
			</a>
		</li>
		<li>
			<a href="" title="Adicionar nova pesquisa">
				Adicionar nova pesquisa
			</a>
		</li>
	</ul>
	<!-- Menu Level 02 -->

	<!-- Menu Level 03 Linhas -->
	<a href="#submenu-03" class="list-group-item" data-toggle="collapse">
		<span class="glyphicon glyphicon-align-justify pull-right"></span> Linhas
	</a>
	<ul id="submenu-03" class="sub-nav collapse in">
		<li>
			<a href="<c:url value='/linhas' />" title="Listar Linhas">
			Lista de linhas
		</a>
	</li>
	<li>
		<a href="<c:url value='/linhas' />" title="Adicionar linha">
			Adicionar nova linha
		</a>
	</li>
	</ul>
	<!-- Menu Level 03 -->


<!-- Menu Level 04 Colunas -->
<a href="#submenu-04" class="list-group-item" data-toggle="collapse">
	<span class="glyphicon glyphicon-th-list pull-right"></span> Colunas
</a>
<ul id="submenu-04" class="sub-nav collapse">
	<li>
		<a href="<c:url value='colunas' />" title="Listar colunas">
		Lista de colunas
	</a>
</li>
<li>
	<a href="<c:url value='colunas' />" title="Adicionar coluna">
	Adicionar nova coluna
</a>
</li>
</ul>
<!-- Menu Level 04 -->

<!-- Menu Level 05 -->
<a href="#submenu-05" class="list-group-item" data-toggle="collapse">
	<span class="glyphicon glyphicon-th pull-right"></span> Tabelas
</a>
<ul id="submenu-05" class="sub-nav collapse">
	<li>
		<a href="<c:url value='tabelas' />" title="Listar Tabelas">
		Lista de tabelas
	</a>
</li>
<li>
	<a href="<c:url value='tabelas' />" title="Adicionar Tabela">
	Adicionar nova tabela
</a>
</li>
</ul>
<!-- Menu Level 05 -->

<!-- Menu Level 06 -->
<a href="#submenu-06" class="list-group-item" data-toggle="collapse">
	<span class="glyphicon glyphicon-user pull-right"></span> Usuarios
</a>
<ul id="submenu-06" class="sub-nav collapse">
	<li>
		<a <a href="<c:url value='/users' />" title="Listar Usuarios">
		Lista de usuarios
	</a>
</li>
<li>
	<a <a href="<c:url value='/users' />" title="Adicionar Usuario">
	Adicionar novo usuario
</a>
</li>
</ul>
<!-- Menu Level 06 -->
</div>
</div>