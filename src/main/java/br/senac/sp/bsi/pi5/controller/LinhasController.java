package br.senac.sp.bsi.pi5.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.senac.sp.bsi.pi5.model.LinhaEntidade;
import br.senac.sp.bsi.pi5.service.LinhaService;

@Controller
@Transactional
public class LinhasController {

	@Autowired
	private LinhaService service;
	
	
	@RequestMapping(value = "/linhas", method = RequestMethod.GET)
	public ModelAndView consultar() {

		ModelAndView mv = new ModelAndView("/linhas/consultar");
		mv.addObject("entidades", service.getColecao());
		
		return mv;
	}

	@RequestMapping(value = "/linhas/digitar", method = RequestMethod.GET)
	public ModelAndView editarLinha(@RequestParam(value = "id",required = false) Integer id) {

		if( id == null )
			id= 0;
		
		ModelAndView mv = new ModelAndView("/linhas/form");
		LinhaEntidade linha = service.getEntidadePorID(id);
		
		if ( linha == null) {

			linha = new LinhaEntidade();

		}
		
		mv.addObject("linha", linha);
		return mv;
	}

	@RequestMapping(value = "/linhas/save", method = RequestMethod.POST)
	public ModelAndView adicionarLinha(@ModelAttribute LinhaEntidade entidade) {

		// Inserir dados 
		if ( entidade.getPkId() == 0  ) {
			service.inserirEntidade(entidade);
		} else {
			// Atualizar os dados
			service.atualizarEntidade(entidade);
		}

		return new ModelAndView("redirect:/linhas");

	}
	
	
	@RequestMapping(value = "/linhas/deletar", method = RequestMethod.GET)
    public ModelAndView deletar(@RequestParam(value = "id",required = true) Integer id) {
        
		service.deletarEntidade(id);
		
		return new ModelAndView("redirect:/linhas");      
    }
}
