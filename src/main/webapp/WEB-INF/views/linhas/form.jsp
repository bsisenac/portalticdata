<jsp:include page="../elementos/header.jsp"/>
				<div class="linhas form">
					<legend>Adicionar Coluna</legend>
					<form:form id="LinhaEditForm" action="save" class="well-ds form-horizontal" commandName="linha" method="post" modelAttribute="linha">
						<div class="form-group required">
							<form:label path="PkId" for="LinhaPkId">
								<spring:message text="ID" />
							</form:label>
							<form:input path="PkId" class="form-control" readonly="true" disabled="true" id="LinhaPkId" />
							<form:hidden path="PkId" required="required" />
						</div>
						<%-- </c:if> --%>
						<div class="form-group required">
							<form:label path="DsLinha" for="LinhaDsLinha">
								<spring:message text="Linha" />
							</form:label>
							<form:input path="DsLinha" class="form-control" id="LinhaDsLinha" />
						</div>

						<button class="btn btn-sm btn-success" type="submit">Salvar</button>
					</form:form>
				</div>
	<jsp:include page="../elementos/footer.jsp"/>