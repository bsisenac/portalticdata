package br.senac.sp.bsi.pi5.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Pesquisas")
public class PesquisaEntidade {
	@Id()
	@Column(name = "PK_ID")
	@GeneratedValue()
	private int PkId;

	@Column(name = "DS_PESQUISA")
	private String DsPesquisa;

	/**
	 * @return the dsPesquisa
	 */
	public String getDsPesquisa() {
		return DsPesquisa;
	}

	/**
	 * @param dsPesquisa
	 *            the dsPesquisa to set
	 */
	public void setDsPesquisa(String dsPesquisa) {
		DsPesquisa = dsPesquisa;
	}

	/**
	 * @return the pkId
	 */
	public int getPkId() {
		return PkId;
	}

	/**
	 * @param pkId
	 *            the pkId to set
	 */
	public void setPkId(int pkId) {
		PkId = pkId;
	}
}
