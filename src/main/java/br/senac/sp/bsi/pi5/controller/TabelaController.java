package br.senac.sp.bsi.pi5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.senac.sp.bsi.pi5.model.TabelaEntidade;
import br.senac.sp.bsi.pi5.service.TabelaService;

@Controller
@Transactional
public class TabelaController {

	@Autowired
	private TabelaService service;
	
	
	@RequestMapping(value = "/tabelas", method = RequestMethod.GET)
	public ModelAndView consultar() {

		ModelAndView mv = new ModelAndView("/tabelas/consultar");
		mv.addObject("entidades", service.getColecao());
		
		return mv;
	}

	@RequestMapping(value = "/tabelas/digitar", method = RequestMethod.GET)
	public ModelAndView editarLinha(@RequestParam(value = "id",required = false) Integer id) {

		if( id == null )
			id= 0;
		
		ModelAndView mv = new ModelAndView("/tabelas/form");
		TabelaEntidade entidade = service.getEntidadePorID(id);
		
		if ( entidade == null) {

			entidade = new TabelaEntidade();

		}
		
		mv.addObject("tabela", entidade);
		return mv;
	}

	@RequestMapping(value = "/tabelas/save", method = RequestMethod.POST)
	public ModelAndView adicionarLinha(@ModelAttribute TabelaEntidade entidade) {

		// Inserir dados 
		if ( entidade.getPkId() == 0  ) {
			service.inserirEntidade(entidade);
		} else {
			// Atualizar os dados
			service.atualizarEntidade(entidade);
		}

		return new ModelAndView("redirect:/tabelas");

	}
	
	
	@RequestMapping(value = "/tabelas/deletar", method = RequestMethod.GET)
    public ModelAndView deletar(@RequestParam(value = "id",required = true) Integer id) {
        
		service.deletarEntidade(id);
		
		return new ModelAndView("redirect:/tabelas");      
    }
}
