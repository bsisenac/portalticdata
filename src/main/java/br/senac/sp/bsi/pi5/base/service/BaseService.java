package br.senac.sp.bsi.pi5.base.service;

import java.util.List;

public interface BaseService<Entidade> {

	// Insere os dados no banco
	public void inserirEntidade(Entidade entidade);

	// Atuaiza os dados de uma entidade
	public void atualizarEntidade(Entidade entidade);

	// Deleta um registro
	public void deletarEntidade(int id);

	// Retornar os dados da pesquisa
	public List<Entidade> getColecao();

	// Retornar os dados da pesquisa
	public Entidade getEntidadePorID(int id);
	
}
