package br.senac.sp.bsi.pi5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.senac.sp.bsi.pi5.base.service.BaseServiceImpl;
import br.senac.sp.bsi.pi5.dao.PesquisaDAO;
import br.senac.sp.bsi.pi5.model.PesquisaEntidade;

@Service
@Transactional

public class PesquisaService extends BaseServiceImpl<PesquisaEntidade> {

	@Autowired
	public PesquisaDAO pesquisaDAO;
	
}
