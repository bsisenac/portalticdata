<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Titulo</title>
<link href="/favicon.ico" type="image/x-icon" rel="icon" />
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<link
	href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
</head>

<body>
	<header>
		<nav class="navbar navbar-default no-print" role="navigation">
			<div class="container">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Navega��o</span> <span class="icon-bar"></span>
							<span class="icon-bar"></span> <span class="icon-bar"></span>
						</button>

						<h1>
							<a href="<c:url value='/' />" class="site-title semi-bold"> <span
								class="bold">Sistema</span>
							</a>
						</h1>
					</div>

					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav pull-right">
							<li><a href="#">Bem-vindo, Nome</a></li>
							<li><a href="/users/logout"> <span
									class="glyphicon glyphicon-log-out"></span> Logout
							</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<div id="container" class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="list-group no-print">
					<!-- Menu Level 02 -->
					<a href="<c:url value='/pesquisas' />" class="list-group-item">
						<span class="glyphicon glyphicon-calendar pull-right"></span>Pesquisas
					</a> <a href="<c:url value='/linhas' />" class="list-group-item"> <span
						class="glyphicon glyphicon-calendar pull-right"></span>Linhas
					</a> <a href="<c:url value='colunas' />" class="list-group-item"> <span
						class="glyphicon glyphicon-calendar pull-right"></span>Colunas <a
						href="<c:url value='tabelas' />" class="list-group-item"> <span
							class="glyphicon glyphicon-calendar pull-right"></span>Tabelas
					</a> <a href="<c:url value='/users' />" class="list-group-item"> <span
							class="glyphicon glyphicon-calendar pull-right"></span>Usuarios
					</a> <a href="<c:url value='/admins' />" class="list-group-item"> <span
							class="glyphicon glyphicon-calendar pull-right"></span>Administradores
					</a> <a href="<c:url value='/categorias' />" class="list-group-item">
							<span class="glyphicon glyphicon-calendar pull-right"></span>Categorias
					</a> <!-- Menu Level 02 -->
				</div>
			</div>
			<div class="col-sm-9">
				<h1>Consulta relacionais</h1>
				<c:if test="${!empty entidades}">
					<table class="table">
						<%-- <c:forEach items="${entidades}" var="entidade">

							<c:if test="${ entidade.getDsColuna() == 'COLUNA' }">

								<tr>
									<th width="150">${coluna.getDsDado()}</th>
								</tr>


							</c:if>

							<c:if test="${ entidade.getDsColuna() != 'COLUNA" }">

								<tr>
									<td width="150">${coluna.getDsDado()}</td>
								</tr>


							</c:if>

							<td width="150">${linha.getDsLinha()}</td>
						</c:forEach> --%>

						<tr>
							<th width="80"></th>
							<c:forEach items="${colunas}" var="entidade">

								<th width="150">${entidade.getDsColuna()}</th>

							</c:forEach>
						</tr>


						<c:forEach items="${entidades}" var="entidade">
							<tr>
								<td width="80">${entidade.getDsLinha()}</td>
								<c:forEach items="${entidades}" var="entidade">
									<td>${entidade.getDsDado()}</td>
								</c:forEach>
							</tr>
						</c:forEach>

						<%-- <tr>
							<th width="150">Coluna</th>
							<th width="150">Linha</th>
							<th width="80">Valor</th>
						</tr>
						<c:forEach items="${entidades}" var="entidade">
							<tr>
								<td>${entidade.getDsColuna()}</td>
								<td>${entidade.getDsLinha()}</td>
								<td>${entidade.getDsDado()}</td>
							</tr>
						</c:forEach> --%>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<footer>
		<div class="rodape">
			<div class="well text-center">
				<div class="col-sm-12">
					<c:if test="${!empty colunas}">
						<table class="table">


						</table>
					</c:if>
				</div>
				<p>
					<b>Rodap�</b>
				</p>
			</div>
		</div>
	</footer>
	<script type="text/javascript"
		src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>