package br.senac.sp.bsi.pi5.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Colunas")
public class ColunaEntidade {
	public ColunaEntidade(){}
	
	public ColunaEntidade( int pkId, String dsColuna ){
		this.setPkId(pkId);
		this.setDsColuna(dsColuna);
	}

	@Id()
	@Column(name = "PK_ID")
	@GeneratedValue()
	private int PkId;

	@Column(name = "DS_COLUNA")
	private String DsColuna;

	/**
	 * @return the pkId
	 */
	public int getPkId() {
		return PkId;
	}

	/**
	 * @param pkId the pkId to set
	 */
	public void setPkId(int pkId) {
		PkId = pkId;
	}

	/**
	 * @return the dsColuna
	 */
	public String getDsColuna() {
		return DsColuna;
	}

	/**
	 * @param dsColuna the dsColuna to set
	 */
	public void setDsColuna(String dsColuna) {
		DsColuna = dsColuna;
	}

}
