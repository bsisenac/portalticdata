<jsp:include page="../elementos/header.jsp"/>
				<div class="pesquisas form">
					<legend>Adicionar Pesquisa</legend>					
					<form:form id="PesquisaEditForm" action="save" class="well-ds form-horizontal" commandName="pesquisa" method="post" modelAttribute="pesquisa">
						<div class="form-group required">
							<form:label path="PkId" for="PesquisaPkId">
								<spring:message text="ID" />
							</form:label>
							<form:input path="PkId" class="form-control" readonly="true" disabled="true" id="PesquisaPkId" />
							<form:hidden path="PkId" required="required" />
						</div>
						<div class="form-group required">
							<form:label path="DsPesquisa" for="PesquisaDsPesquisa">
								<spring:message text="Pesquisa" />
							</form:label>
							<form:input path="DsPesquisa" class="form-control" id="PesquisaDsPesquisa" />
						</div>

						<button class="btn btn-success" type="submit">Salvar</button>
					</form:form>
				</div>
				<jsp:include page="../elementos/footer.jsp"/>