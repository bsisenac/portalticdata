<jsp:include page="../elementos/header.jsp"/>
				<div class="tabelas form">

					<form:form id="TabelaEditForm" action="save" class="well-ds form-horizontal" commandName="tabela" method="post" modelAttribute="tabela">
						<div class="form-group required">
							<form:label path="PkId" for="TabelaPkId">
								<spring:message text="ID" />
							</form:label>
							<form:input path="PkId" class="form-control" readonly="true" disabled="true"
								id="TabelaPkId" />
							<form:hidden path="PkId" required="required" />
						</div>
						
						<div class="form-group required">
							<form:label path="DsTabela" for="TabelaDsTabela">
								<spring:message text="Tabela" />
							</form:label>
							<form:input path="DsTabela" class="form-control" id="TabelaDsTabela" />
						</div>
						
						<div class="form-group required">
							<form:label path="FkLinha" for="TabelaFkLinha">
								<spring:message text="Id.Linha" />
							</form:label>
							<form:input path="FkLinha" class="form-control" id="TabelaFkLinha" />
						</div>
						
						<div class="form-group required">
							<form:label path="FkColuna" for="TabelaFkColuna">
								<spring:message text="Id.Coluna" />
							</form:label>
							<form:input path="FkColuna" class="form-control" id="TabelaFkColuna" />
						</div>
						
						<div class="form-group required">
							<form:label path="DsDado" for="TabelaDsDado">
								<spring:message text="Valor" />
							</form:label>
							<form:input path="DsDado" class="form-control" id="TabelaDsDado" />
						</div>
						
						<div class="form-group required">
							<form:label path="FkPesquisa" for="TabelaFkPesquisa">
								<spring:message text="Id.Pesquisa" />
							</form:label>
							<form:input path="FkPesquisa" class="form-control" id="TabelaFkPesquisa" />
						</div>
						
						<button class="btn btn-success" type="submit">Salvar</button>
					</form:form>
				</div>
				<jsp:include page="../elementos/footer.jsp"/>