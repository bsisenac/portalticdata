<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Titulo</title>
<link href="/favicon.ico" type="image/x-icon" rel="icon" />
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bootstrap/css/main.css" rel="stylesheet" />
</head>

<body>
	<header>
		<nav class="navbar navbar-default no-print" role="navigation">
			<div class="container">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Navegação</span> <span class="icon-bar"></span>
							<span class="icon-bar"></span> <span class="icon-bar"></span>
						</button>

						<h1>
							<a href="<c:url value='/' />" class="site-title semi-bold"> <span class="bold">Sistema</span>
							</a>
						</h1>
					</div>

					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav pull-right">
							<li><a href="#">Bem-vindo, Nome</a></li>
							<li><a href="/users/logout"> 
								<span class="glyphicon glyphicon-log-out"></span> Logout
							</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<div id="container" class="container">
		<div class="row">
			<jsp:include page="../menu.jsp"/>
