package br.senac.sp.bsi.pi5.base.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.senac.sp.bsi.pi5.base.dao.BaseDAO;


public class BaseServiceImpl<Entidade> implements BaseService<Entidade>{

	@Autowired
	private BaseDAO<Entidade> entidadeDAO;
	
	@Override
	public void inserirEntidade(Entidade entidade) {
		entidadeDAO.inserirEntidade(entidade);
	}

	@Override
	public void atualizarEntidade(Entidade entidade) {
		entidadeDAO.atualizarEntidade(entidade);
	}

	@Override
	public void deletarEntidade(int id) {
		entidadeDAO.deletarEntidade(id);
		
	}

	@Override
	public List<Entidade> getColecao() {
		return entidadeDAO.getColecao();
	}

	/**
	 * @return the entidadeDAO
	 */
	public BaseDAO<Entidade> getEntidadeDAO() {
		return entidadeDAO;
	}

	/**
	 * @param entidadeDAO the entidadeDAO to set
	 */
	public void setEntidadeDAO(BaseDAO<Entidade> entidadeDAO) {
		this.entidadeDAO = entidadeDAO;
	}

	@Override
	public Entidade getEntidadePorID(int id) {
		
		return this.entidadeDAO.getEntidadePorID(id);
	}

}
