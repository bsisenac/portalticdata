package br.senac.sp.bsi.pi5.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.senac.sp.bsi.pi5.base.service.BaseService;
import br.senac.sp.bsi.pi5.base.service.BaseServiceImpl;
import br.senac.sp.bsi.pi5.dao.PesquisaDAO;
import br.senac.sp.bsi.pi5.model.PesquisaEntidade;

public class ImportXLSX {

	int nColInicial = 2, nColFinal = 2;
	int nLinInicial = 4;
	
	private PesquisaDAO pesquisaDAO;
	
	
	public ImportXLSX(){
		
	}
	
	public void ExibirTabela(XSSFSheet mySheet) {

		String col = "", agruCol = "", lin = "", agruLin = "", dado = "", aux = "", pesquisa = "";
		
		// Get iterator to all the rows in current sheet
		Iterator<Row> rowIterator = mySheet.iterator();

		// Traversing over each row of XLSX file
		while (rowIterator.hasNext()) {

			Row row = rowIterator.next();

			// For each row, iterate through each columns
			Iterator<Cell> cellIterator = row.cellIterator();

			while (cellIterator.hasNext()) {

				Cell cell = cellIterator.next();

				// Total
				if (cell.getColumnIndex() == 0 && cell.getRowIndex() == 3 && cell.getStringCellValue().isEmpty()) {
					this.nLinInicial++;
					this.nColFinal++;
				}

				 if (cell.getColumnIndex() == 0 && cell.getRowIndex() == 0)
					 gravarPesquisa(cell.getStringCellValue());
				
				// Linhas
				if (cell.getRowIndex() >=  this.nLinInicial) {

					aux = separarAgrupamentoLinha(cell);
					if( ! aux.isEmpty() )
						agruLin= aux;
					
					aux = separarLinha(cell);
					if( ! aux.isEmpty() )
						lin= aux;

				}

				// Colunas
				if (cell.getRowIndex() >= this.nColInicial && cell.getRowIndex() <= this.nColFinal) {

					aux = separarAgrupamentoColuna(cell);
					if( ! aux.isEmpty() )
						agruCol= aux;
					
					aux = separarColuna(cell);
					if( ! aux.isEmpty() )
						col = aux;

				}
				
				col = col.replace("\n", "/");
				lin = lin.replace("\n", "/");
				agruLin = agruLin.replace("\n", "/");
				agruCol = agruCol.replace("\n", "/");
				
				if( ! col.isEmpty() && ! lin.isEmpty() )
				{
//					System.out.println( "Agru. Col: " + col + ", Col: " + agruCol + ", Agru. Lin: " + agruLin + ",Lin:" + lin);					
					
				}
				
			}
		}

	}

	private String separarAgrupamentoLinha(Cell cell) {

		String retorno = "";

		if (cell.getColumnIndex() == 0)
			if (!cell.getStringCellValue().isEmpty())
				retorno = cell.getStringCellValue();

		return retorno;

	}

	private String separarLinha(Cell cell) {

		String retorno = "";

		if (cell.getColumnIndex() == 1)
			if (!cell.getStringCellValue().isEmpty())
				retorno = cell.getStringCellValue();

		return retorno;

	}

	private String separarAgrupamentoColuna(Cell cell) {

		String retorno = "";

		if( cell.getRowIndex() == this.nColInicial )
		if (!cell.getStringCellValue().isEmpty())
			retorno = cell.getStringCellValue();

		return retorno;

	}

	private String separarColuna(Cell cell) {

		String retorno = "";

		
		if( cell.getRowIndex() == this.nColFinal )
			if (!cell.getStringCellValue().isEmpty())
				retorno = cell.getStringCellValue();

		return retorno;

	}

	public void Importar() throws IOException {

		File myFile = new File("tabelas.xlsx");

		FileInputStream fis = new FileInputStream(myFile);

		// Finds the workbook instance for XLSX file
		XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);

		// Return first sheet from the XLSX workbook
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);

		this.ExibirTabela(mySheet);

		myWorkBook.close();
	}
	
	private int gravarPesquisa( String pesquisa ){
		
		
		PesquisaEntidade entidade = new PesquisaEntidade();
		entidade.setDsPesquisa(pesquisa);
		entidade.setPkId(0);
		try{
			if( this.pesquisaDAO == null )
//				System.out.println( "Erro: Null" );
		pesquisaDAO.inserirEntidade(entidade);
					
		}catch( Exception o ){
			
			System.out.println( "Erro aqui: " + o.getMessage() );
		}
//		
		return 0;
	}

}
//// Título
// if (nCol == 0 && nRow == 0)
// System.out.print("Título: " + cell.getStringCellValue() +
// "\n");
//
// // Sub. Título
// if (nCol == 0 && nRow == 1)
// System.out.print("Sub. Título: " + cell.getStringCellValue()
// + "\n");
//
// // Primeira coluna
// if (nCol == 0 && nRow == 2)
// System.out.print("Primeira coluna: " +
// cell.getStringCellValue() + "\n");
//