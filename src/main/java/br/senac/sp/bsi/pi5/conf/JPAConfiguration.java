package br.senac.sp.bsi.pi5.conf;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Esta classe contém as informações de confguração do banco de dados.
 * <P>
 * A annotation @Bean nos métodos é para indicar que os objetos criados por eles 
 * vão ser gerenciados pelo Spring e podem ser injetados em qualquer ponto do código.
 * 
 * 
 * <P>
 * A annotation @EnableTransactionManagement indica que o Spring deve utilizar o controle transacional.
 * 
 */
@EnableTransactionManagement
public class JPAConfiguration {

	/**
	 * Este método informa qual a implementação será utilizada.
	 * 
	 * @param emf
	 * @return
	 */
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager ();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
	
	/**
	 * A classe LocalContainerEntityManagerFactoryBean é a abstração do arquivo persistence.xml.
	 * <P>
	 * A classe HibernateJpaVendorAdapter representa a escolha de implementação da JPA, o Hibernate.
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { "br.senac.sp.bsi.pi5.model" });

		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}

	private Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		properties.setProperty("hibernate.show_sql", "true");
		return properties;
	}

	/**
	 * Configura os parâmetros de conexão com o banco de dados.
	 */
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl("jdbc:mysql://localhost:3306/pi5");
		dataSource.setUsername("equipe2");
		//dataSource.setPassword("123456");
		dataSource.setPassword("123456");
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		return dataSource;
	}

}
