<jsp:include page="../elementos/headerLogin.jsp"/>
<div class="users form">
	<h2 class="center">Sistema</h2>
	<form action="/users/login" class="card signin-card form-horizontal" id="UserLoginForm" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>			
		<fieldset class="fieldset-login">
		<legend class="center">Acesso ao Sistema</legend>
			<div class="form-group">
				<div class="col col-md-12">
					<input name="data[User][username]" class="form-control" placeholder="Insira seu e-mail" maxlength="50" type="text" id="UserUsername"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col col-md-12">
					<input name="data[User][password]" class="form-control" placeholder="Insira sua senha" type="password" id="UserPassword"/>
				</div>
			</div>
			<div class="col col-md-12">
				<div class="form-group">
					<button class="btn btn-lg btn-success btn-block" type="submit" value="Entrar"><strong>ENTRAR <span class=""></span></strong></button>
				</div>
			</div>
		</fieldset>
	</form>		
</div>
<jsp:include page="../elementos/footer.jsp"/>