package br.senac.sp.bsi.pi5.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Tabelas")
public class TabelaEntidade {
	@Id()
	@Column(name = "PK_ID")
	@GeneratedValue()
	private int PkId;

	@Column(name = "DS_TABELA")
	private String DsTabela;

	@Column(name = "DS_DADO")
	private String DsDado;

	@Column(name = "FK_LINHA")
	private int FkLinha;

	@Column(name = "FK_COLUNA")
	private int FkColuna;

	@Column(name = "FK_PESQUISA")
	private int FkPesquisa;

	/**
	 * @return the pkId
	 */
	public int getPkId() {
		return PkId;
	}

	/**
	 * @param pkId
	 *            the pkId to set
	 */
	public void setPkId(int pkId) {
		PkId = pkId;
	}

	/**
	 * @return the dsTabela
	 */
	public String getDsTabela() {
		return DsTabela;
	}

	/**
	 * <<<<<<< HEAD
	 * 
	 * @param dsTabela
	 *            the dsTabela to set =======
	 * @param dsTabela
	 *            the dsTabela to set >>>>>>>
	 *            6514ae0b3dfe15039f832cc2028ea6268ed6d347
	 */
	public void setDsTabela(String dsTabela) {
		DsTabela = dsTabela;
	}

	/**
	 * @return the fkLinha
	 */
	public int getFkLinha() {
		return FkLinha;
	}

	/**
	 * @param fkLinha
	 *            the fkLinha to set
	 */
	public void setFkLinha(int fkLinha) {
		FkLinha = fkLinha;
	}

	/**
	 * @return the fkColuna
	 */
	public int getFkColuna() {
		return FkColuna;
	}

	/**
	 * 
	 * @param fkColuna 
	 */
	public void setFkColuna(int fkColuna) {
		FkColuna = fkColuna;
	}

	/**
	 * @return the dsDado
	 */
	public String getDsDado() {
		return DsDado;
	}

	public void setDsDado(String dsDado) {
		DsDado = dsDado;
	}

	/**
	 * @return the fkPesquisa
	 */
	public int getFkPesquisa() {
		return FkPesquisa;
	}


	public void setFkPesquisa(int fkPesquisa) {
		FkPesquisa = fkPesquisa;
	}
}
