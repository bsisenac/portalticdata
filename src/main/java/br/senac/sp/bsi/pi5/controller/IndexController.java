package br.senac.sp.bsi.pi5.controller;

import java.io.File;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.senac.sp.bsi.pi5.service.ColunaService;
import br.senac.sp.bsi.pi5.service.ImportXLSX;

@Controller
@Transactional
public class IndexController {

	private ImportXLSX xlsxService;
	
	@RequestMapping("/home")
	public String index() {
		System.out.println("Carrengando Produtos");
		return "/pesquisas";
	}

	@RequestMapping("/Importar")
	public String teste() {

		xlsxService = new ImportXLSX();

		try {
			
			File out = new File(".");
			System.out.println( out.getAbsolutePath() );
			
			xlsxService.Importar();
			
		} catch (Exception o) {
			System.out.printf("Erro: " + o.getMessage());

//			o.printStackTrace();
		}

		return "redirected:/";
	}
}
