package br.senac.sp.bsi.pi5.dao;

import org.springframework.stereotype.Repository;

import br.senac.sp.bsi.pi5.base.dao.BaseDAOImpl;
import br.senac.sp.bsi.pi5.model.TabelaEntidade;

@Repository
public class TabelaDAO extends BaseDAOImpl<TabelaEntidade>{
	
}
