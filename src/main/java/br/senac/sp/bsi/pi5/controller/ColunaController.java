package br.senac.sp.bsi.pi5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.senac.sp.bsi.pi5.model.ColunaEntidade;
import br.senac.sp.bsi.pi5.service.ColunaService;

@Controller
@Transactional
public class ColunaController {

	@Autowired
	private ColunaService service;
	
	
	@RequestMapping(value = "/colunas", method = RequestMethod.GET)
	public ModelAndView consultarColuna() {

		ModelAndView mv = new ModelAndView("/colunas/consultar");
		mv.addObject("entidades", service.getColecao());
		
		return mv;
	}

	@RequestMapping(value = "/colunas/digitar", method = RequestMethod.GET)
	public ModelAndView editarColuna(@RequestParam(value = "id",required = false) Integer id) {

		if( id == null )
			id= 0;
		
		ModelAndView mv = new ModelAndView("/colunas/form");
		ColunaEntidade entidade = service.getEntidadePorID(id);
		
		if ( entidade == null) {

			entidade = new ColunaEntidade();

		}
		
		mv.addObject("coluna", entidade);
		return mv;
	}

	@RequestMapping(value = "/colunas/save", method = RequestMethod.POST)
	public ModelAndView adicionarColuna(@ModelAttribute ColunaEntidade entidade) {

		// Inserir dados 
		if ( entidade.getPkId() == 0  ) {
			service.inserirEntidade(entidade);
		} else {
			// Atualizar os dados
			service.atualizarEntidade(entidade);
		}

		return new ModelAndView("redirect:/colunas");

	}
	
	
	@RequestMapping(value = "/colunas/deletar", method = RequestMethod.GET)
    public ModelAndView deletarColuna(@RequestParam(value = "id",required = true) Integer id) {
        
		service.deletarEntidade(id);
		
		return new ModelAndView("redirect:/colunas");      
    }
	
}
