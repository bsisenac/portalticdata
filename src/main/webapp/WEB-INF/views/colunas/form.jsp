<jsp:include page="../elementos/header.jsp"/>
				<div class="colunas form">
					<legend>Adicionar Coluna</legend>
					<form:form id="ColunaEditForm" action="save" class="well-ds form-horizontal" commandName="coluna" method="post" modelAttribute="coluna">
						<div class="form-group required">
							<form:label path="PkId" for="ColunaPkId">
								<spring:message text="ID" />
							</form:label>
							<form:input path="PkId" readonly="true" disabled="true" class="form-control" id="ColunaPkId" />
							<form:hidden path="PkId" required="required" />
						</div>
						<%-- </c:if> --%>
						<div class="form-group required">
							<form:label path="DsColuna" for="ColunaDsColuna">
								<spring:message text="Coluna" />
							</form:label>
							<form:input path="DsColuna" class="form-control" id="ColunaDsColuna" />
						</div>

						<button class="btn btn-sm btn-success" type="submit">Salvar</button>
					</form:form>
				</div>
	<jsp:include page="../elementos/footer.jsp"/>