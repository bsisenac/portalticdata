package br.senac.sp.bsi.pi5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senac.sp.bsi.pi5.base.service.BaseServiceImpl;
import br.senac.sp.bsi.pi5.dao.TabelaDAO;
import br.senac.sp.bsi.pi5.model.TabelaEntidade;

@Service
public class TabelaService extends BaseServiceImpl<TabelaEntidade>{

	@Autowired
	public TabelaDAO tabelaDAO;
	
}
