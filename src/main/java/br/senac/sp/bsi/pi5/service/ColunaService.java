package br.senac.sp.bsi.pi5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senac.sp.bsi.pi5.base.service.BaseServiceImpl;
import br.senac.sp.bsi.pi5.dao.ColunaDAO;
import br.senac.sp.bsi.pi5.model.ColunaEntidade;

@Service
public class ColunaService extends BaseServiceImpl<ColunaEntidade> {

	@Autowired
	public ColunaDAO colunaDAO;
}
