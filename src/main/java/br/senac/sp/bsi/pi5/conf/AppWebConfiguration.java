package br.senac.sp.bsi.pi5.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.senac.sp.bsi.pi5.base.dao.BaseDAO;
import br.senac.sp.bsi.pi5.base.service.BaseService;
import br.senac.sp.bsi.pi5.controller.PesquisasController;
import br.senac.sp.bsi.pi5.dao.LinhaDAO;
import br.senac.sp.bsi.pi5.dao.PesquisaDAO;
import br.senac.sp.bsi.pi5.dao.TabelaDinamicaDAO;
import br.senac.sp.bsi.pi5.model.TabelaDinamicaEntidade;
import br.senac.sp.bsi.pi5.service.LinhaService;
import br.senac.sp.bsi.pi5.service.PesquisaService;
import br.senac.sp.bsi.pi5.service.TabelaDinamicaService;

/**
 * O objetivo principal desta classe é expor para a Servlet do Spring MVC quais
 * são as classes que devem ser lidas e carregadas.
 *
 * <P>
 * A annotation @ComponentScan serve para indicar quais pacotes devem ser lidos.
 * A classe foi passada como parâmetro, mas o Spring identifica o pacote da
 * classe.
 * 
 * <P>
 * A annotation @EnableWebMvc tem várias funções.
 */
@EnableWebMvc
//@ComponentScan(basePackageClasses = { FarmaciaPopularController.class,
//		FarmaciaPopularService.class, FarmaciaPopularConverter.class,
//		FarmaciaPopularDAO.class,BaseDAO.class,BaseDAOImpl.class })

@ComponentScan(basePackageClasses = { PesquisasController.class,
	BaseDAO.class, BaseService.class,PesquisaService.class,PesquisaDAO.class,LinhaService.class,LinhaDAO.class,
	TabelaDinamicaDAO.class, TabelaDinamicaEntidade.class, TabelaDinamicaService.class})
	
public class AppWebConfiguration extends WebMvcConfigurerAdapter {

	/**
	 * Este método retorna um objeto cuja responsabilidade é informar onde as
	 * páginas devem ser encontradas.
	 * 
	 * <P>
	 * A classe InternalResourceViewResolver guarda as configurações da pasta e
	 * do sufixo que devem ser adicionados para qualquer caminho que seja
	 * retornado por métodos dos controllers.
	 * 
	 * <P>
	 * A annotation @Bean indica para o Spring que o retorno desse método deve
	 * ser registrado como um objeto gerenciado pelo container.
	 * 
	 * @return
	 */
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		
		return resolver;
	}
	
	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}

	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
}
