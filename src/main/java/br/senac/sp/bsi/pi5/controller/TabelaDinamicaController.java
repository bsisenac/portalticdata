package br.senac.sp.bsi.pi5.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.senac.sp.bsi.pi5.model.ColunaEntidade;
import br.senac.sp.bsi.pi5.model.LinhaEntidade;
import br.senac.sp.bsi.pi5.model.TabelaDinamicaEntidade;
import br.senac.sp.bsi.pi5.model.TabelaEntidade;
import br.senac.sp.bsi.pi5.service.ColunaService;
import br.senac.sp.bsi.pi5.service.LinhaService;
import br.senac.sp.bsi.pi5.service.TabelaDinamicaService;
import br.senac.sp.bsi.pi5.service.TabelaService;

@Controller
@Transactional
public class TabelaDinamicaController {

	@Autowired
	private TabelaDinamicaService service;

	@Autowired
	private ColunaService colunaService;

	@Autowired
	private LinhaService linhaService;

	@RequestMapping(value = "/portalCliente", method = RequestMethod.GET)
	public ModelAndView consultar() {

		List<ColunaEntidade> colunas;
		List<LinhaEntidade> linhas;

		int nQtCol = 0, nQtLin = 0;

		// Dados da tabela juntos
		List<TabelaDinamicaEntidade> dados = service.getColecao();

		// Separa as colunas do sistema
		colunas = this.distinctColuna(dados);

		// Separa as linhas do sistema
		linhas = this.distinctLinha(dados);

		// Quantida de colunas
		nQtCol = colunas.size();

		// Quantidade de linhas
		nQtLin = linhas.size();
		
		//
		String[][] matriz = this.criarMatrizDinamica(nQtLin, nQtCol, colunas, dados);

		exibirMatriz( matriz, nQtLin, nQtCol );
		
		ModelAndView mv = new ModelAndView("/portalCliente/consultar");

		mv.addObject("entidades", dados);
		mv.addObject("tabela", matriz);
		mv.addObject("qtLinha", nQtLin);
		mv.addObject("qtCol", nQtCol);

		return mv;

	}

	private String[][] criarMatrizDinamica(int nQtLinha, int nQtColuna, List<ColunaEntidade> colunas,
			List<TabelaDinamicaEntidade> dados) {
			String[][] matriz = new String[nQtLinha + 1][nQtColuna + 1];

		int i, j, contador;

		j = 1;
		contador = 0;

		for (ColunaEntidade coluna : colunas) {

			matriz[0][j] = coluna.getDsColuna();
			j++;

		}

		for (i = 1; i < nQtLinha; i++) {

			for (j = 0; j < nQtColuna; j++) {

				if (j == 0)
					matriz[i][j] = dados.get(contador).getDsColuna();
				else {
					matriz[i][j] = dados.get(contador).getDsDado();
					if (contador <= dados.size())
						contador++;

				}

			}
		}

		return matriz;
	}

	private ArrayList<LinhaEntidade> distinctLinha(List<TabelaDinamicaEntidade> tabela) {

		ArrayList<LinhaEntidade> linhas = new ArrayList<LinhaEntidade>();

		for (TabelaDinamicaEntidade dado : tabela) {

			boolean encontrou = false;

			for (LinhaEntidade linha : linhas) {

				if (linha.getDsLinha().equals(dado.getDsLinha())) {
					encontrou = true;
					break;
				}

			}

			if (!encontrou) {

				linhas.add(new LinhaEntidade(dado.getFkLinha(), dado.getDsLinha()));

			}

		}

		return linhas;
	}

	private ArrayList<ColunaEntidade> distinctColuna(List<TabelaDinamicaEntidade> tabela) {

		ArrayList<ColunaEntidade> colunas = new ArrayList<ColunaEntidade>();

		for (TabelaDinamicaEntidade dado : tabela) {

			boolean encontrou = false;

			for (ColunaEntidade coluna : colunas) {

				if (coluna.getDsColuna().equals(dado.getDsLinha())) {
					encontrou = true;
					break;
				}

			}

			if (!encontrou) {

				colunas.add(new ColunaEntidade(dado.getFkLinha(), dado.getDsLinha()));

			}
		}

		return colunas;
	}

	public void exibirMatriz(String[][] matriz, int nQtLin, int nQtCol ){
		
		System.out.println("Qt.Linha: " + nQtLin+ ", Qt.Col: " + nQtCol);
		for (int i = 0; i < nQtLin; i++) {
		
			for (int j = 0; j < nQtCol; j++) {
				
				System.out.print("|\t");
				System.out.print(matriz[i][j]);
			}
			System.out.print("|\n");
		}
		
	}
	
	
}

// PUBLIC MATRIZ[2,4]
// I = 2
// J = 2
//
// MATRIZ[1,1] = ''
//
// SELE TMPCOL
// GO top
// SCAN WHILE NOT EOF()
// MATRIZ[1,J] = ALLTRIM( TMPCOL.COLUNA )
// J = J + 1
// ENDSCAN
//
// SELE TMPTESTE
// GO TOP
//
// FOR I = 2 TO 2
// FOR J = 1 TO 4
// IF( J = 1 )
// MATRIZ[I,J] = TMPTESTE.LINHA
// ELSE
// MATRIZ[I,J] = TMPTESTE.DADO
// IF ! EOF()
// SELE TMPTESTE
// SKIP
// ELSE
// EXIT
// ENDIF
// ENDIF
// ENDFOR
// ENDFOR
//
// FOR I = 1 TO 2
// FOR J = 1 TO 4
// ?? ' ' +ALLTRIM( MATRIZ[I,J] )
// ENDFOR
// ?
// ENDFOR
