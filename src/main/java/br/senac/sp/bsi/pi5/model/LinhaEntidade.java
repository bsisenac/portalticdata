package br.senac.sp.bsi.pi5.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Linhas")
public class LinhaEntidade {
	
	public LinhaEntidade(){	}
	
	public LinhaEntidade( int pkId, String dsLinha ){
		this.setPkId(pkId);
		this.setDsLinha(dsLinha);
	}
	
	@Id()
	@Column(name = "PK_ID")
	@GeneratedValue()
	private int PkId;

	@Column(name = "DS_LINHA")
	private String DsLinha;

	/**
	 * @return the pkId
	 */
	public int getPkId() {
		return PkId;
	}

	/**
	 * @param pkId the pkId to set
	 */
	public void setPkId(int pkId) {
		PkId = pkId;
	}

	/**
	 * @return the dsLinha
	 */
	public String getDsLinha() {
		return DsLinha;
	}

	/**
	 * @param dsLinha the dsLinha to set
	 */
	public void setDsLinha(String dsLinha) {
		DsLinha = dsLinha;
	}

}
