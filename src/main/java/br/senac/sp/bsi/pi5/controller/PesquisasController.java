package br.senac.sp.bsi.pi5.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.senac.sp.bsi.pi5.model.PesquisaEntidade;
import br.senac.sp.bsi.pi5.service.PesquisaService;

@Controller
@Transactional
public class PesquisasController {

	@Autowired
	private PesquisaService service;

	@RequestMapping(value = "/pesquisas", method = RequestMethod.GET)
	public ModelAndView Consultar() {
		
		ModelAndView mv = new ModelAndView("/pesquisas/consultar");
		mv.addObject("entidades", service.getColecao());		
		return mv;
	}

	@RequestMapping(value = "/pesquisas/digitar", method = RequestMethod.GET)
	public ModelAndView editarPesquisa(@RequestParam(value = "id",required = false) Integer id) {

		if( id == null )
			id= 0;
		
		ModelAndView mv = new ModelAndView("/pesquisas/form");
		PesquisaEntidade pes = service.getEntidadePorID(id);
		
		if ( pes == null) {

			pes = new PesquisaEntidade();

		}
		
		System.out.println( "Pk_ID: " + pes.getPkId() + ", Pesquisa: " + pes.getDsPesquisa() );
		
		mv.addObject("pesquisa", pes);
		return mv;
	}

	@RequestMapping(value = "/pesquisas/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute PesquisaEntidade pesquisa) {

		// Inserir dados 
		if ( pesquisa.getPkId() == 0  ) {
			service.inserirEntidade(pesquisa);
		} else {
			// Atualizar os dados
			service.atualizarEntidade(pesquisa);
		}

		return new ModelAndView("redirect:/pesquisas");

	}
	
	@RequestMapping(value = "/pesquisas/deletar", method = RequestMethod.GET)
    public ModelAndView deletar(@RequestParam(value = "id",required = true) Integer id) {
        
		service.deletarEntidade(id);
		
		return new ModelAndView("redirect:/pesquisas");      
    }
}
