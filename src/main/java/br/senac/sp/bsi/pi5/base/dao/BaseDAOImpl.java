package br.senac.sp.bsi.pi5.base.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class BaseDAOImpl<Entidade> implements BaseDAO<Entidade> {

	@PersistenceContext
	private EntityManager manager;
	
	private Class<Entidade> persistentClass;
	
	@SuppressWarnings("unchecked")
	private Class<Entidade> instanciarClassePersistent(){
		return persistentClass = (Class<Entidade>) 
				( (ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}
	@Override
	public void inserirEntidade(Entidade entidade) {
		manager.persist(entidade);
	}

	@Override
	public void atualizarEntidade(Entidade entidade) {
		manager.merge(entidade);
	}

	public void deletarEntidade(Entidade entidade) {

		manager.remove(entidade);
		
	}

	public void deletarEntidade(int id) {
		Entidade ent = this.getEntidadePorID(id);
		this.deletarEntidade(ent);
	}

	@Override
	public List<Entidade> getColecao() {
		
		this.persistentClass = this.instanciarClassePersistent();
		
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<Entidade> cq = cb.createQuery(persistentClass);
		cq.from(persistentClass);
		List<Entidade> list = (List<Entidade>) manager.createQuery(cq).getResultList();
		return list;
	}
	
	@Override
	public Entidade getEntidadePorID(int id) {

		this.persistentClass = this.instanciarClassePersistent();
		
		return (Entidade) manager.find(persistentClass, id);

	}

}
