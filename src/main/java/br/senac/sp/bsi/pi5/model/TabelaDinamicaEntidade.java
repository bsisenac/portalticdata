package br.senac.sp.bsi.pi5.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Tabela_Dinamica")
public class TabelaDinamicaEntidade {

	@Id
	@Column(name = "PK_ID")
	private int PkId;

	@Column(name = "DS_PESQUISA")
	private String DsPesquisa;

	@Column(name = "DS_COLUNA")
	private String DsColuna;

	@Column(name = "DS_LINHA")
	private String DsLinha;
	
	@Column(name = "DS_DADO")
	private String DsDado;
	
	@Column(name = "FK_LINHA")
	private int FkLinha;

	/**
	 * @return the pkId
	 */
	public int getPkId() {
		return PkId;
	}

	/**
	 * @param pkId the pkId to set
	 */
	public void setPkId(int pkId) {
		PkId = pkId;
	}

	/**
	 * @return the dsPesquisa
	 */
	public String getDsPesquisa() {
		return DsPesquisa;
	}

	/**
	 * @param dsPesquisa the dsPesquisa to set
	 */
	public void setDsPesquisa(String dsPesquisa) {
		DsPesquisa = dsPesquisa;
	}

	/**
	 * @return the dsColuna
	 */
	public String getDsColuna() {
		return DsColuna;
	}

	/**
	 * @param dsColuna the dsColuna to set
	 */
	public void setDsColuna(String dsColuna) {
		DsColuna = dsColuna;
	}

	/**
	 * @return the dsLinha
	 */
	public String getDsLinha() {
		return DsLinha;
	}

	/**
	 * @param dsLinha the dsLinha to set
	 */
	public void setDsLinha(String dsLinha) {
		DsLinha = dsLinha;
	}

	/**
	 * @return the dsDado
	 */
	public String getDsDado() {
		return DsDado;
	}

	/**
	 * @param dsDado the dsDado to set
	 */
	public void setDsDado(String dsDado) {
		DsDado = dsDado;
	}

	/**
	 * @return the fkLinha
	 */
	public int getFkLinha() {
		return FkLinha;
	}

	/**
	 * @param fkLinha the fkLinha to set
	 */
	public void setFkLinha(int fkLinha) {
		FkLinha = fkLinha;
	}
}
