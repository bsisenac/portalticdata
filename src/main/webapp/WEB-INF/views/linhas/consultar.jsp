<jsp:include page="../elementos/header.jsp"/>
<div class="col-sm-9">
<div class="linhas index">
	<h2>Lista de linhas</h2>
	<div class="panel panel-default">
		<div class="panel-heading">
		</div>
		<c:if test="${!empty entidades}">
		<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed">
				<thead>
					<tr>
						<th width="80">ID</th>
						<th width="150">Linha</th>
						<th width="80">Edit</th>
						<th width="80">Delete</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${entidades}" var="entidade">
					<tr>
						<td>${entidade.getPkId()}</td>
						<td>${entidade.getDsLinha()}</td>
						<td><a
							href="<c:url value='/linhas/digitar?id=${entidade.getPkId()}' />">Edit</a></td>
							<td><a
								href="<c:url value='/linhas/deletar?id=${entidade.getPkId()}' />">Delete</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:if>
		</div>
	</div>
	<div class="row">
		<div class="actions col-xs-12">
			<a class="btn btn-sm btn-primary" href="<c:url value='/linhas/digitar' />">Adicionar linha <span class=""></span></a> 
<jsp:include page="../elementos/footer.jsp"/>